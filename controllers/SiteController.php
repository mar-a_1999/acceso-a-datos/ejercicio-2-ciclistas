<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Etapa;
use app\models\Puerto;
use app\models\Ciclista;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(*) as Ciclistas"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista;",
            
        ]);
    } //HECHA CORRECTAMENTE.
    public function actionConsulta1(){
        //MEDIANTE DAO
        $numero = Yii::$app->db
                    ->createCommand("select COUNT(*) as Ciclistas FROM ciclista")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' =>"select COUNT(*) as Ciclistas FROM ciclista",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
                "campos"=>['Ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS ciclistas FROM ciclista;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta2a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(*) as Ciclistas_Banesto") ->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Ciclistas_Banesto'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) AS Número_Ciclistas_Equipo_Banesto FROM ciclista WHERE nomequipo='Banesto';
;",
            
        ]);
    } //HECHA CORRECTAMENTE.
    public function actionConsulta2(){
        //MEDIANTE DAO
      $numero = Yii::$app->db
                    ->createCommand("SELECT COUNT(*) as Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto'")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(*) as Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto'",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Ciclistas_Banesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) AS Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto';",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta3a(){
     //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad) as Edad_Media"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>"SELECT AVG(edad) AS Edad_Media FROM ciclista;",
      
            
        ]);
 
} //HECHA CORRECTAMENTE.
    public function actionConsulta3(){
        //MEDIANTE DAO
          $numero = Yii::$app->db
                    ->createCommand("SELECT AVG(edad) as Edad_Media FROM ciclista")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad) as Edad_Media FROM ciclista",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Edad_Media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>"SELECT AVG(edad) AS Edad_Media FROM ciclista;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta4a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad) as Edad_Media_Ciclistas_Banesto") ->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Edad_Media_Ciclistas_Banesto'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los ciclistas del equipo Banesto",
            "sql"=>"SELECT AVG(edad) AS Edad_Media_Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto';",
      
            
        ]);
 
} //HECHA CORRECTAMENTE.
    public function actionConsulta4(){
        //MEDIANTE DAO
          $numero = Yii::$app->db
                    ->createCommand("SELECT AVG(edad) Edad_Media_Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto'")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad) Edad_Media_Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto'",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Edad_Media_Ciclistas_Banesto'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los ciclistas del equipo Banesto",
            "sql"=>"SELECT AVG(edad) AS Edad_Media_Ciclistas_Banesto FROM ciclista WHERE nomequipo='Banesto';",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta5a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad) as Edad_Media_Por_Equipo, nomequipo") ->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Edad_Media_Por_Equipo','nomequipo'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) AS Edad_Media_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo;" , 
      
            
        ]);
 
} //HECHA CORRECTAMENTE.
    public function actionConsulta5(){
          //MEDIANTE DAO
          $numero = Yii::$app->db
                    ->createCommand("SELECT AVG(edad) as Edad_Media_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad) as Edad_Media_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo",
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Edad_Media_Por_Equipo','nomequipo'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) AS Edad_Media_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta6a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(nombre) as Numero_Ciclistas_Por_Equipo, nomequipo") ->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Numero_Ciclistas_Por_Equipo', 'nomequipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(nombre) as Numero_Ciclistas_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo;", 
      
            
        ]);
 
    } //HECHA CORRECTAMENTE.
    public function actionConsulta6(){
           //MEDIANTE DAO
          $numero = Yii::$app->db
                    ->createCommand("SELECT COUNT(nombre) as Numero_Ciclistas_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(nombre) as Numero_Ciclistas_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo",
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Numero_Ciclistas_Por_Equipo','nomequipo'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(nombre) as Numero_Ciclistas_Por_Equipo, nomequipo FROM ciclista GROUP BY nomequipo;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta7a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("COUNT(*) as Número_Total_Puertos")
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Número_Total_Puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) AS Número_Total_Puertos FROM puerto;", 
      
            
        ]);
 
    } //HECHA CORRECTAMENTE.
    public function actionConsulta7(){
      //MEDIANTE DAO
        $numero = Yii::$app->db
                    ->createCommand("SELECT COUNT(*) AS Número_Total_Puertos FROM puerto")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(*) AS Número_Total_Puertos FROM puerto",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Número_Total_Puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) AS Número_Total_Puertos FROM puerto;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta8a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("COUNT(*) as Puertos_Mayores_De_1500") ->where("altura > 1500"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Puertos_Mayores_De_1500'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) AS Puertos_Mayores_De_1500 FROM puerto WHERE altura > 1500;", 
      
            
        ]);
 
    } //HECHA CORRECTAMENTE.
    public function actionConsulta8(){
        //MEDIANTE DAO
         $numero = Yii::$app->db
                    ->createCommand("SELECT COUNT(*) AS Puertos_Mayores_De_1500 FROM puerto WHERE altura > 1500")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(*) AS Puertos_Mayores_De_1500 FROM puerto WHERE altura > 1500",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Puertos_Mayores_De_1500'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) AS Puertos_Mayores_De_1500 FROM puerto WHERE altura > 1500;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta9a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(nombre) as Equipos_Mayores_De_Cuatro_Ciclistas, nomequipo") ->groupBy("nomequipo") ->having("COUNT(nombre)>4"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipos_Mayores_De_Cuatro_Ciclistas','nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo, count(nombre) AS Equipos_Mayores_De_Cuatro_Ciclistas FROM ciclista GROUP BY nomequipo HAVING count(nombre)>4;", 
      
            
        ]);
 
    } //HECHA CORRECTAMENTE.
    public function actionConsulta9(){
      //MEDIANTE DAO
         $numero = Yii::$app->db
                    ->createCommand("SELECT COUNT(nombre) as Equipos_Mayores_De_Cuatro_Ciclistas, nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(nombre)>4")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(nombre) as Equipos_Mayores_De_Cuatro_Ciclistas, nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(nombre)>4",
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipos_Mayores_De_Cuatro_Ciclistas','nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT COUNT(nombre) as Equipos_Mayores_De_Cuatro_Ciclistas, nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(nombre)>4;",
            
        ]);
    }  //HECHA CORRECTAMENTE.
    public function actionConsulta10a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(nombre) as Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad, nomequipo") ->where("edad BETWEEN 28 and 32") ->groupBy("nomequipo") ->having("COUNT(nombre)>4"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad','nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT COUNT(nombre) Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad, nomequipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING COUNT(nombre) > 4;", 
      
            
        ]);
 
    }//HECHA CORRECTAMENTE.
    public function actionConsulta10(){
         //MEDIANTE DAO
         $numero = Yii::$app->db
                    ->createCommand("SELECT COUNT(nombre) Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad, nomequipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING COUNT(nombre) > 4")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(nombre) Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad, nomequipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING COUNT(nombre) > 4",
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad','nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"-- (10) Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT COUNT(nombre) Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad, nomequipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING COUNT(nombre) > 4",
            
        ]);
    } //HECHA CORRECTAMENTE.
    public function actionConsulta11a(){
        //Mediante active record
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find() ->select("dorsal, COUNT(*) as Etapas_ganadas") ->groupBy("dorsal"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal','Etapas_ganadas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) AS Etapas_ganadas FROM etapa GROUP BY dorsal;", 
      
            
        ]);
 
    }//HECHA CORRECTAMENTE.
    public function actionConsulta11(){
        //MEDIANTE DAO
         $numero = Yii::$app->db
                    ->createCommand("SELECT dorsal, COUNT(*) Etapas_ganadas FROM etapa GROUP BY dorsal")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal, COUNT(*) Etapas_ganadas FROM etapa GROUP BY dorsal",
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal','Etapas_ganadas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) Etapas_ganadas FROM etapa GROUP BY dorsal;",
            
        ]);
    } //HECHA CORRECTAMENTE.
    public function actionConsulta12a(){
        //Mediante active record
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find() ->select("dorsal") ->groupBy("dorsal") ->having("COUNT(*)>1"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING(COUNT(*)>1);", 
      
            
        ]);
 
    }//HECHA CORRECTAMENTE.
    public function actionConsulta12(){
        //MEDIANTE DAO
         $numero = Yii::$app->db
                    ->createCommand("SELECT dorsal FROM etapa GROUP BY dorsal HAVING(COUNT(*)>1)")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM etapa GROUP BY dorsal HAVING(COUNT(*)>1)",
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING(COUNT(*)>1)",
            
        ]);
    } //HECHA CORRECTAMENTE.
    
}
