<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciclista".
 *
 * @property int $dorsal
 * @property string $nombre
 * @property int|null $edad
 * @property string $nomequipo
 *
 * @property Equipo $nomequipo0
 * @property Etapa[] $etapas
 * @property Lleva[] $llevas
 * @property Puerto[] $puertos
 */
class Ciclista extends \yii\db\ActiveRecord
{
    
    public $Ciclistas;
    public $Ciclistas_Banesto;
    public $Edad_Media;
    public $Edad_Media_Ciclistas_Banesto;
    public $Edad_Media_Por_Equipo;
    public $Numero_Ciclistas_Por_Equipo;
    public $Equipos_Mayores_De_Cuatro_Ciclistas;
    public $Equipos_Mayores_De_Cuatro_Ciclistas_Entre_28_32_edad;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciclista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dorsal', 'nombre', 'nomequipo'], 'required'],
            [['dorsal', 'edad'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['nomequipo'], 'string', 'max' => 25],
            [['dorsal'], 'unique'],
            [['nomequipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipo::className(), 'targetAttribute' => ['nomequipo' => 'nomequipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dorsal' => 'Dorsal',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'nomequipo' => 'Nomequipo',
        ];
    }

    /**
     * Gets query for [[Nomequipo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomequipo0()
    {
        return $this->hasOne(Equipo::className(), ['nomequipo' => 'nomequipo']);
    }

    /**
     * Gets query for [[Etapas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEtapas()
    {
        return $this->hasMany(Etapa::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::className(), ['dorsal' => 'dorsal']);
    }
}
